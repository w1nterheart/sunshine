package com.exiri.vi.sunshine.app.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.exiri.vi.sunshine.app.MainActivity;
import com.exiri.vi.sunshine.app.R;

/**
 * Created by exiri on 10/2/14.
 */
public class WeatherAppWidgetProvider extends AppWidgetProvider{
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int i = 0; i < appWidgetIds.length; i++) {
            int appWidgetId = appWidgetIds[i];

            //Intent for a launch
            Intent intent = new Intent(context, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

            //Create view and setOnClick
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_main_layout);
            views.setOnClickPendingIntent(R.id.widget_btn_update, pendingIntent);

            //Update on current widget
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }
}
